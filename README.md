Comment installer le site :

Nous avons utilisé MySQL.
Les fichiers à installer sont tables.sql et insertions.sql
Ces fichiers sont dans le répertoire BD_Script.

A la racine du projet il y a un fichier connect.php dans lequel il y a le chemin pour se connecter à la BD. Il faudra donc modifier le fichier si vous ne voulez pas utiliser notre base de données.

Le répertoire contenant le projet doit être installé dans le www afin d'avoir accès à notre site depuis votre localhost.



Les différentes fonctionnalités du site :

La page d'accueil (index) permet de visualiser 10 monuments de la base de donnée. Il s'agit des dix premiers (par ordre de références dans la BD)

La page d'ajout (ajouter un Monument) permet d'ajouter un monument à la BD. Malheureusement nous avons quelques problèmes de cohérence avec la BD. Notre choix concernant la façon de choisir le département dans lequel se situe le monument n'est pas le plus judicieux. Il aurait été préférable d'insérer la liste des départements et leur numéro dans la base de donnée afin que l'utilisateur n'ai qu'à selectionner dans une liste déroulate. (plutôt que de rentrer manuellement le nom du département puis son numéro ce qui cause des redondances d'informations).
L'ajout d'une image ne fonctionne pas encore (partie non débeugée par manque de temps).


Dans l'onglet "lister les monuments" : Les monuments sont simplement listés. Il est possible (tout comme à partir de l'index) d'afficher les détails du monument en cliquant sur l'image ou sur son nom.

L'onglet "Rechercher" permet de faire une recherche par :
      Région (lister tous les monuments de la région)
      Nom de département (lister tous les monuments du département)
      Nom de Commune (lister tous les monuments de la ville)
      Nom de monument (si plusieurs monuments ont le même nom alors ils seront listés et l'utilisateur peut choisir celui qu'il veut)


Partout dans le site :
  lorsque les monuments sont visibles au format image+nom+autre il est possible de cliquer dessus pour accéder à sa fiche de détail.
  il est possible de revenir à l'accueil à tout moment en cliquand sur "Accueil" dans la barre de menu

Pour accéder directement à notre site depuis les machines de l'IUT :

http://localhost/~champdaveine/Projet-Monuments-Historiques/Index.php

Sinon pour l'installer, vous avez accès à notre projet sur gitlab :

https://gitlab.com/ManonC/Projet-Monuments-Historiques.git
