<!DOCTYPE html>
<html lang='fr'>
<head>
	<meta charset='utf-8'/>
	<title>Recherche de Monument</title>
	<link rel='stylesheet' href='Index.css'/>
</head>
<body>
	<?php

	require 'Connexion.php';
	include 'bandeau.html' ;

	$connexion = ConnexionMySQL();
	$stmt="";

/*Permet d'afficher une recherche par région*/
  if(!empty($_POST['region']) && $_POST['recherche']=="region"){
  	$sql = "SELECT * FROM OBJET NATURAL JOIN COMMUNE NATURAL JOIN DEPARTEMENT NATURAL JOIN REGION
  	WHERE nomRegion LIKE ? ORDER BY nomRegion";
  	//LIMIT 25 OFFSET ".$GLOBALS['cpt'];
  	$stmt = $connexion -> prepare($sql);
  	$stmt->execute(array("%$_POST[region]%"));
  }

  /*Permet d'afficher une recherche par Département*/
  if(!empty($_POST['Dpt']) && $_POST['recherche']=="Dpt"){
     $sql = "SELECT * FROM OBJET NATURAL JOIN COMMUNE NATURAL JOIN DEPARTEMENT
     WHERE nomDpt LIKE ? ORDER BY nomDpt";
     //LIMIT 25 OFFSET ".$GLOBALS['cpt'];
     $stmt = $connexion -> prepare($sql);
     $stmt->execute(array("%$_POST[Dpt]%"));
  }


  /*Permet d'afficher une recherche par Commune*/
  if(!empty($_POST['ville']) && $_POST['recherche']=="ville"){
     $sql = "SELECT * FROM OBJET NATURAL JOIN COMMUNE
     WHERE nomCommune LIKE ? ORDER BY nomCommune";
     //LIMIT 25 OFFSET ".$GLOBALS['cpt'];
     $stmt = $connexion -> prepare($sql);
     $stmt->execute(array("%$_POST[ville]%"));
  }

   /*Permet d'afficher une recherche par nom de monument*/
   if(!empty($_POST['objet']) && $_POST['recherche']=="objet"){
      $sql = "SELECT * FROM OBJET
      WHERE nomObjet LIKE ? ORDER BY nomObjet";
      //LIMIT 25 OFFSET ".$GLOBALS['cpt'];
      $stmt = $connexion -> prepare($sql);
      $stmt->execute(array("%$_POST[objet]%"));
   }

	/* affichage du résultat */

  if (!$stmt) echo "Pb d'accès aux Monuments";
  else{
  	$nb_ligne = $stmt->rowCount();
  	if ($nb_ligne==0) echo "Inconnu !<br/>";
  	else{
  		echo "<table id = 'monuments'>";
            $cpt = 0;
  		foreach ($stmt as $row){
  			if($cpt == 0){
  				echo "<tr>";
  			}
  			elseif($cpt%5 == 0){
  				echo "</tr><tr>";
  			}
  			$cpt += 1;

  			echo "<td><a href = 'FicheMonument.php?refMonument=".$row['refMonument']."&&nomObjet=".$row['nomObjet']."'><img src = ./ImagesMonuments/".$row['nomImage']." height=200 width=150/><br/>".$row['nomObjet']."</a></td>";
  		}
  		echo"</tr></table>";
  	 }

    foreach ($stmt as $row)
        echo $row['nomObjet']."<br/>";
  }



  	?>
</body>
</html>
