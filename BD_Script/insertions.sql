insert into REGION values ('Alsace');
insert into REGION values ('Bourgogne');
insert into REGION values ('Bretagne');
insert into REGION values ('Languedoc-Roussillon');
insert into REGION values ('Franche-Comté');

insert into DEPARTEMENT values (68, 'Haut-Rhin', 'Alsace');
insert into DEPARTEMENT values (21, "Côte-d'or", 'Bourgogne');
insert into DEPARTEMENT values (56, 'Morbihan', 'Bretagne');
insert into DEPARTEMENT values (34, 'Hérault', 'Languedoc-Roussillon');
insert into DEPARTEMENT values (25, 'Doubs', 'Franche-Comté');

insert into COMMUNE values ('Mulhouse', 68);
insert into COMMUNE values ('Tailly', 21);
insert into COMMUNE values ('Sussey', 21);
insert into COMMUNE values ('Priziac', 56);
insert into COMMUNE values ('Port-Louis', 56);
insert into COMMUNE values ('Saint-Nazaire-de-Ladarez', 34);
insert into COMMUNE values ('Saint-Michel', 34);
insert into COMMUNE values ('Saint-Maurice-Navacelles', 34);
insert into COMMUNE values ('Saint-Mathieu-de-Tréviers', 34);
insert into COMMUNE values ('Saint-Martin-de-Londres', 34);
insert into COMMUNE values ('Anteuil', 25);
insert into COMMUNE values ('Amagney', 25);


insert into OBJET values (1,
						  "Musée national de l'Automobile",
						  'Berline',
						  'Limousine A H de la collection Schlumpf',
						  '',
						  'Minerva (usine)',
						  '2ème quart 20ème siècle',
						  '04/14/1978',
						  'Propriété publique',
						  '1.jpg',
						  'Mulhouse');

insert into OBJET values (2,
						  'Eglise paroissiale Saint-Fiacre',
						  'Statue',
						  "Statue : Vierge à l'Enfant",
						  'Bois : peint',
						  'inconnu',
						  '18ème siècle',
						  '30/12/1988',
						  'Propriété de la commune',
						  '2.jpg',
						  'Tailly');

insert into OBJET values (3,
						  'Eglise paroissiale Saint-Pierre-Saint-Paul',
						  'Tableau',
						  "Tableau : saint François d'Assise ",
						  "Bois (support) : peinture à l'huile",
						  'inconnu',
						  '17ème siècle',
						  '14/10/1988',
						  'Propriété de la commune',
						  '3.jpg',
						  'Sussey');

insert into OBJET values (4,
						  'Eglise Saint-Beheau',
						  'Groupe sculpté',
						  "Groupe sculpté : saint Roch ",
						  "Bois",
						  'inconnu',
						  'Limite 15ème siècle 16ème siècle',
						  '25/03/1924',
						  'Propriété de la commune',
						  '4.jpg',
						  'Priziac');

insert into OBJET values (5,
						  'Eglise Notre-Dame',
						  'Reliquaire',
						  "Reliquaire de la Vraie Croix ",
						  "Bois : doré",
						  'Philippe (sculpteur)',
						  '3ème quart 18ème siècle',
						  '05/04/1966',
						  'Propriété de la commune',
						  '5.jpg',
						  'Port-Louis');


insert into OBJET values (6,
						  'Beffroi communal',
						  'Cloche',
						  '',
						  "Bronze",
						  'Serva Pie (fondeur de cloches)',
						  '2ème quart 18ème siècle',
						  '12/07/1957',
						  'Propriété de la commune',
						  '6.jpg',
						  'Saint-Nazaire-de-Ladarez');

insert into OBJET values (7,
						  'Eglise',
						  'Chaire à prêcher',
						  '',
						  "Pierre : taillée",
						  'inconnu',
						  '4ème quart 17ème siècle',
						  '29/07/1966',
						  'Propriété de la commune',
						  '7.jpg',
						  'Saint-Martin-de-Londres');

insert into OBJET values (8,
						  "Eglise de l'Assomption",
						  'Croix',
						  "Croix : Christ en croix",
						  'Bois : taillé, peint',
						  'inconnu',
						  '17ème siècle',
						  '06/01/1975',
						  'Propriété de la commune',
						  '8.jpg',
						  'Anteuil');

insert into OBJET values (9,
						  "Eglise Saint-Ferréol et Saint-Ferjeux",
						  'Autel, retable',
						  "Autel, retable (autel secondaire)",
						  'Pierre : taillé',
						  'inconnu',
						  '19ème siècle',
						  '30/08/1984',
						  'Propriété de la commune',
						  '9.jpg',
						  'Amagney');
