<?php
require "Connexion.php";
$connexion=ConnexionMySQL();
error_reporting(E_ALL);
ini_set('display_error', 1);

if (!empty($_GET['nomObjet'])){
    $nomObjet=$_GET['nomObjet'];
}
else{
    $nomObjet=NULL;
}

if (!empty($_GET['nomRegion'])){
    $nomRegion=$_GET['nomRegion'];
}
else{
    $nomRegion=NULL;
}

if (!empty($_GET['nomDpt'])){
    $nomDpt=$_GET['nomDpt'];
}
else{
    $nomDpt=NULL;

}


if (!empty($_GET['numeroDept'])){
    $numeroDept=$_GET['numeroDept'];
}
else{
    $numeroDept=NULL;
}

if (!empty($_GET['nomCommune'])){
    $nomCommune=$_GET['nomCommune'];
}
else{
    $nomCommune=NULL;
}

if (!empty($_GET['nomEdifice'])){
    $nomEdifice=$_GET['nomEdifice'];
}
else{
    $nomEdifice=NULL;
}

if (!empty($_GET['auteur'])){
    $auteur=$_GET['auteur'];
}
else{
    $auteur=NULL;
}


if (!empty($_GET['descriptionObjet'])){
    $descriptionObjet=$_GET['descriptionObjet'];
}
else{
    $descriptionObjet=NULL;
}

if (!empty($_GET['materiaux'])){
    $materiaux=$_GET['materiaux'];
}
else{
    $materiaux=NULL;
}

if (!empty($_GET['siecle'])){
    $siecle=$_GET['siecle'];
}
else{
    $siecle=NULL;
}

if (!empty($_GET['status'])){
    $status=$_GET['status'];
}
else{
    $status=NULL;
}

if (!empty($_GET['dateAjout'])){
    $dateAjout=$_GET['dateAjout'];
}
else{
    $dateAjout=NULL;
}


try {
	$connexion = null;
	$connexion = ConnexionMySQL();
	$insert = "INSERT INTO REGION VALUES (?)";
	$stmt=$connexion->prepare($insert);
  $stmt->bindValue(1,$nomRegion);
	$stmt->execute();
}

catch (PDOException $Exception) {}

try {
	$connexion = null;
	$connexion = ConnexionMySQL();
	$insert = "INSERT INTO DEPARTEMENT VALUES (?, ?, ?)";
	$stmt=$connexion->prepare($insert);
  $stmt->bindValue(1,$numeroDept);
  $stmt->bindValue(2,$nomDpt);
  $stmt->bindValue(3,$nomRegion);
	$stmt->execute();
}
catch (PDOException $Exception) {}

try {
	$connexion = null;
	$connexion = ConnexionMySQL();
	$insert = "INSERT INTO COMMUNE VALUES (?, ?)";
	$stmt=$connexion->prepare($insert);
	$stmt->bindValue(1,$nomCommune);
	$stmt->bindValue(2,$numeroDept);
	$stmt->execute();
}

  catch (PDOException $Exception) {}

$image = "default.png";

/*
if (!empty($_FILES["image"]["name"])) {
  $image = $_FILES["image"]["name"];
  $tmps_name=$_FILES["image"]["tmp_name"];
  $chemin="./ImagesMonuments/";
  move_uploaded_file($tmps_name,$chemin.basename($_FILES["image"]["name"]));
}*/




$insert = "INSERT INTO OBJET (nomEdifice, nomObjet, descriptionObjet, materiaux, auteur, siecle, dateAjout, status, nomImage, nomCommune) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
$stmt=$connexion->prepare($insert);
$stmt->execute(array($nomEdifice, $nomObjet, $descriptionObjet, $materiaux, $auteur, $siecle, $dateAjout, $status, $image, $nomCommune));
$connexion = null;
$connexion = ConnexionMySQL();

header("Location: Index.php");

?>
